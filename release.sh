#!/usr/bin/env bash
# unofficial bash strict mode
# https://archive.fo/pINgs#selection-169.0-201.0
set -euo pipefail
IFS=$'\n\t'

# Adapted from https://packaging.python.org/tutorials/packaging-projects/

SCRIPT_DIR=`dirname "$0"`

cd $SCRIPT_DIR

if [[ -e dist ]] ; then
    echo "Clearing old releases"
    rm -rf dist
fi

echo
echo "Generating distributions"
python3 setup.py sdist bdist_wheel

echo
echo "Starting upload"
twine upload dist/*
